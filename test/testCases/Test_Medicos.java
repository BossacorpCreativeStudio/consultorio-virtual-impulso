/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testCases;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import entidades.ent_Medico;
import modelos.md_Medicos;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Casos de prueba para el modelo y entidad de Médico
 * @author jaycorpstudios
 * @version 1
 */
@RunWith(JUnit4.class)
public class Test_Medicos {
    
    private md_Medicos modelo;
    private ent_Medico medico;
    private ent_Medico medicoTest;
    
    public Test_Medicos() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        modelo = new md_Medicos();
        //Registro del primer medico en la base de datos
        medico = new ent_Medico(1, "Manuel", "Zaragoza", "Martinez", " Martinez", "Lunes a Viernes 07:00 - 15:00 ", "mzaragoza@consultorio_virtual.com");
        medicoTest = modelo.getMedico(1);
    }
    
    @After
    public void tearDown() {
    }

        @Test
        public void testGetMedicos(){
            assertNotNull(modelo.getMedicos());
        }
    
        @Test
        public void testIdMedico(){
            
            int idEsperado = medico.getIdUsuario();
                       
            //Comprobar ID
            assertEquals(idEsperado, medicoTest.getIdUsuario());
        }
        
        @Test
        public void testNombreMedico(){
            
            String nombreEsperado = medico.getNombreCompleto();
            
            //Comprobar Nombre
            assertEquals(nombreEsperado, medicoTest.getNombreCompleto());
        }
        
        @Test
        public void testCamposObligatoriosMedico(){
            //Comprobar que los campos obligatorios no sean null
            assertNotNull(medicoTest.getEspecialidad());
            assertNotNull(medicoTest.getContacto());
            assertNotNull(medicoTest.getHorario());
        }
    
}
