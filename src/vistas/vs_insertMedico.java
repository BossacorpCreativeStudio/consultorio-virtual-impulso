import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class vs_insertMedico extends JFrame {

	private JPanel contentPane;
	private JTextField txt_name;
	private JTextField txt_espec;
	private JTextField txt_email;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vs_insertMedico frame = new vs_insertMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public vs_insertMedico() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAgregarMdico = new JLabel("Agregar M\u00E9dico");
		lblAgregarMdico.setBounds(165, 11, 93, 23);
		contentPane.add(lblAgregarMdico);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(57, 50, 62, 14);
		contentPane.add(lblNombre);
		
		JLabel lblEspecialidad = new JLabel("Especialidad:");
		lblEspecialidad.setBounds(37, 75, 82, 14);
		contentPane.add(lblEspecialidad);
		
		txt_name = new JTextField();
		txt_name.setBounds(129, 47, 129, 20);
		contentPane.add(txt_name);
		txt_name.setColumns(10);
		
		txt_espec = new JTextField();
		txt_espec.setBounds(128, 72, 253, 20);
		contentPane.add(txt_espec);
		txt_espec.setColumns(10);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
	
				JOptionPane.showMessageDialog(null,"Agregado con exito");
			}
		});
		btnAgregar.setBounds(150, 166, 126, 40);
		contentPane.add(btnAgregar);
		
		JLabel lblCorreoElectrnico = new JLabel("Correo Electr\u00F3nico:");
		lblCorreoElectrnico.setBounds(7, 98, 126, 14);
		contentPane.add(lblCorreoElectrnico);
		
		txt_email = new JTextField();
		txt_email.setBounds(129, 95, 251, 20);
		contentPane.add(txt_email);
		txt_email.setColumns(10);
	}
}
