/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelos;

import entidades.ent_Medico;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Clase para el manejo de datos de los medicos
 * @author jaycorpstudios
 */
public class md_Medicos extends ConexionBd{
    
    /**
     * Metodo para obtener todos los medicos de la base de datos, retorna un listado con entidades de medicos por cada registro en la base de datos
     * @
     * @return LinkedList<ent_Medico>
     */
    public LinkedList<ent_Medico> getMedicos(){
        
        LinkedList<ent_Medico> medicos = new LinkedList<>();
        ResultSet resultados = null;
        
        this.conexion = this.conectar();
        try {
            preparedStatement = conexion.prepareStatement("SELECT * FROM Medicos");
            resultados = preparedStatement.executeQuery();
            //Convertir resultados a un listado de ent_medicos
            while(resultados.next()){
                 ent_Medico medico = new ent_Medico(resultados.getInt("id"),
                                                    resultados.getString("nombre"),
                                                    resultados.getString("aPaterno"),
                                                    resultados.getString("aMaterno"),
                                                    resultados.getString("especialidad"),
                                                    resultados.getString("horario"),
                                                    resultados.getString("contacto")
                                                   );
                 //Guardamos el objeto en una lista
                 medicos.add(medico);
            }
            
        } catch (SQLException ex) {
            //Logger.getLogger(ent_Usuario.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex);
        }
             
        return medicos;
    }
    /**
     * Metodo para obtener un medico en función a su Id
     * @param Integer idMedico
     * @return 
     */
    public ent_Medico getMedico(Integer idMedico){
        ent_Medico medico = new ent_Medico();
        ResultSet resultados = null;
        
        this.conexion = this.conectar();
        try {
            preparedStatement = conexion.prepareStatement("SELECT * FROM Medicos WHERE id = ?");
            preparedStatement.setInt(1, idMedico);
            resultados = preparedStatement.executeQuery();
            //Convertir resultados a un listado de ent_medicos
            while(resultados.next()){
                 medico = new ent_Medico(resultados.getInt("id"),
                                resultados.getString("nombre"),
                                resultados.getString("aPaterno"),
                                resultados.getString("aMaterno"),
                                resultados.getString("especialidad"),
                                resultados.getString("horario"),
                                resultados.getString("contacto")
                               );
            }
            
        } catch (SQLException ex) {
            //Logger.getLogger(ent_Usuario.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex);
        }
             
        return medico;
    }
    
}
