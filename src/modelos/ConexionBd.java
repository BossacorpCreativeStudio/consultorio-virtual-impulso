package modelos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConexionBd{
    
    private String servidor = "localhost:3306";
    private String nombreBaseDeDatos = "consultorio";
    private String usuario = "root";
    private String password = "";
    
    //propiedades para la conexion con mySQL
    protected Connection conexion = null;
    protected Statement statement = null;
    protected PreparedStatement preparedStatement = null;
    protected ResultSet resultSet = null;
    
    /**
     * Conectar()
     * Realiza una conexion a la base de datos
     */
    protected Connection conectar(){
        
        //Base de datos metro y tabla lineas
        try{
            //Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://"+servidor+"/"+nombreBaseDeDatos+"?user="+usuario+"&password="+password);
            
        }catch(SQLException excepcion){
            //Mostrar errores
            System.out.println("SQLException: " + excepcion.getMessage());
            System.out.println("SQLState: " + excepcion.getSQLState());
            System.out.println("VendorError: " + excepcion.getErrorCode());
        }
        return conexion;
    }
}