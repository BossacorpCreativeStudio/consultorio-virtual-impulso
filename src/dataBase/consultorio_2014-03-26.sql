# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.44)
# Database: consultorio
# Generation Time: 2014-03-27 00:50:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Medicos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Medicos`;

CREATE TABLE `Medicos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `aPaterno` varchar(255) DEFAULT NULL,
  `aMaterno` varchar(255) DEFAULT NULL,
  `especialidad` varchar(255) DEFAULT NULL,
  `horario` varchar(255) DEFAULT NULL,
  `contacto` varchar(255) DEFAULT 'medicos@consultorio_virtual.com',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `Medicos` WRITE;
/*!40000 ALTER TABLE `Medicos` DISABLE KEYS */;

INSERT INTO `Medicos` (`id`, `nombre`, `aPaterno`, `aMaterno`, `especialidad`, `horario`, `contacto`)
VALUES
	(1,'Manuel','Zaragoza','Martinez','Nutriologo','Lunes a Viernes 07:00 - 15:00 ','mzaragoza@consultorio_virtual.com'),
	(2,'Daniela','Benitez','Godinez','Cirugía Estetica','Lunes a Viernes 07:00 - 15:00 ','dbenitez@consultorio_virtual.com'),
	(3,'Pablo','Escobar','Garza','Medico General','Lunes a Sabado 07:00 - 18:00 ','pescobar@consultorio_virtual.com');

/*!40000 ALTER TABLE `Medicos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
