/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

/**
 *
 * @author jaycorpstudios
 */
public class ent_Medico {
    
    private int idUsuario;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombreUsuario;
    private String horario;
    private String especialidad;
    private String contacto;
    
    public ent_Medico(){
    
    }
    
    public ent_Medico(Integer id, String nombre, String apellidoPaterno, String apellidoMaterno, String especialidad, String horario, String contacto){
        this.idUsuario = id;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.especialidad = especialidad;
        this.contacto = contacto;
        this.horario = horario;
    }
    
    public void setNombreCompleto(String nombre, String apellidoPaterno, String apellidoMaterno){
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
    }
    
    public void setEspecialidad(String especialidad){
        this.especialidad = especialidad;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }
    
    public String getNombreCompleto(){
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public String getContacto() {
        return contacto;
    }
    
    public String getHorario(){
        return this.horario;
    }
}
